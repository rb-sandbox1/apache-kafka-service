# Apache Kafka Service

## Description
Very simple service that uses different parts of Apache Kafka to produce and consume messages via topics. Requires Kafka environment (zookeper) to run.

## Technologies used
- Java 17
- Spring Boot 3.1
- Spring Kafka
- Lombok
