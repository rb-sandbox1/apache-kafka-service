package com.example.kafka.dto;

public record MessageDTO(String message) {
}
